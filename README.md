# IDS-721-Cloud-Computing :computer:

## Mini Project 8 :page_facing_up: 

## :ballot_box_with_check: Requirements
* Rust command-line tool
* Data ingestion/processing
* Unit tests

## :ballot_box_with_check: Grading Criteria
* __Tool functionality__ 30%
* __Data processing__ 30%
* __Testing implementation__ 30%
* __Documentation__ 10%

## :ballot_box_with_check: Deliverables
* Rust tool source code
* Sample output
* Testing report

## :ballot_box_with_check: Main Progress
* Function to Sum the Numbers
```rust
use std::env;
use std::fs::File;
use std::io::{self, BufRead};

// Function to read integers from a file and calculate their sum
fn calculate_sum_from_file(file_path: &str) -> Result<i32, io::Error> {
    let file = File::open(file_path)?;
    let reader = io::BufReader::new(file);
    let mut sum = 0;

    for line in reader.lines() {
        let num_str = line?;
        let num = num_str.trim().parse::<i32>().unwrap_or(0);
        sum += num;
    }

    Ok(sum)
}

fn main() {
    // Get file path from command-line arguments
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <file_path>", args[0]);
        std::process::exit(1);
    }
    let file_path = &args[1];

    // Calculate sum from file and print result
    match calculate_sum_from_file(file_path) {
        Ok(sum) => println!("Sum of numbers in file: {}", sum),
        Err(e) => eprintln!("Error: {}", e),
    }
}

#[cfg(test)]
mod test;
```
1. __`Data ingestion`__: The data ingestion process begins with the program retrieving the file path from the command-line arguments. This is done using the std::env::args function, which returns an iterator over the command-line arguments. The program expects exactly one argument (besides the program name): the path to the file containing integers.
- In this project, data is extracted from the 'test_input.txt' file, and the function sums up only the numbers contained within it.
```
100
-20
30
abc
40
-10
```

2. __`Data Processing`__: The function performs data processing by reading and summing numbers from a 'test_input.txt' file. It iterates over each line, converting strings to integers, and ignores non-numeric values by treating them as zeros. This process ensures only valid numbers contribute to the total sum, showcasing an efficient method of extracting and aggregating numerical data from a text file.

3. __`Rust Command-Line Tool`__: The Rust command-line tool processes a file specified by the user, summing up all numerical values contained within. It showcases Rust's ability to efficiently handle file I/O and parse data, demonstrating a practical application of Rust for creating robust and performant command-line utilities.

* Output of the function by rust command-line tool
![Result](https://github.com/suim-park/Machine-Learning-Assignment-1/assets/143478016/22d2440c-ece3-4238-b687-ba5a66bdef89) 

4. __`Unit Tests`__: The test function compares the output of the 'calculate_sum_from_file' function against 140, which is the expected sum of all numbers in the 'test_input.txt' file, to verify its accuracy.

```rust
// src/test.rs

use super::*; // Import everything from the parent module

#[test]
fn test_calculate_sum_correctly() {
    // Define the path to your test file
    // Adjust the path as necessary if the file is located elsewhere
    let file_path = "test_input.txt"; // Ensure this file exists and is accessible
    
    // Call the function with the path to the test file
    let result = calculate_sum_from_file(file_path);
    
    // Assert that the result is Ok(140)
    assert_eq!(result.unwrap(), 140);
}
```

* Test Function has passed.
![Test](https://github.com/suim-park/Machine-Learning-Assignment-1/assets/143478016/fba5815c-09a9-4bc3-97af-a8816b82586e)