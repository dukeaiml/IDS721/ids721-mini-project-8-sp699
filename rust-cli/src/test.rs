// src/test.rs

use super::*; // Import everything from the parent module

#[test]
fn test_calculate_sum_correctly() {
    // Define the path to your test file
    // Adjust the path as necessary if the file is located elsewhere
    let file_path = "test_input.txt"; // Ensure this file exists and is accessible
    
    // Call the function with the path to the test file
    let result = calculate_sum_from_file(file_path);
    
    // Assert that the result is Ok(140)
    assert_eq!(result.unwrap(), 140);
}
