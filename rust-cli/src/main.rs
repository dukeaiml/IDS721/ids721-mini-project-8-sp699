use std::env;
use std::fs::File;
use std::io::{self, BufRead};

// Function to read integers from a file and calculate their sum
fn calculate_sum_from_file(file_path: &str) -> Result<i32, io::Error> {
    let file = File::open(file_path)?;
    let reader = io::BufReader::new(file);
    let mut sum = 0;

    for line in reader.lines() {
        let num_str = line?;
        let num = num_str.trim().parse::<i32>().unwrap_or(0);
        sum += num;
    }

    Ok(sum)
}

fn main() {
    // Get file path from command-line arguments
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: {} <file_path>", args[0]);
        std::process::exit(1);
    }
    let file_path = &args[1];

    // Calculate sum from file and print result
    match calculate_sum_from_file(file_path) {
        Ok(sum) => println!("Sum of numbers in file: {}", sum),
        Err(e) => eprintln!("Error: {}", e),
    }
}

#[cfg(test)]
mod test;